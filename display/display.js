
function onClose(id) {
    document.getElementById(id).classList.remove('show');
}

window.onbeforeunload = function () {
    console.log('Call to disconnect API...');
    return true;
};

document.addEventListener("visibilitychange", function() {
    if (document.hidden){
        console.log("Browser tab is hidden")
    } else {
        console.log("Browser tab is visible")
    }
});